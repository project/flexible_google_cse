<?php

namespace Drupal\flexible_google_cse\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class FlexibleSearchController.
 */
class FlexibleSearchController extends ControllerBase {

  /**
   * Search.
   *
   * @return string
   *   Return SearchResult
   */
  public function fgcsSearch() {
    $searchService = \Drupal::service('flexible_google_cse_search');
    $searchResult = $searchService->search();
    $searchResult['#weight'] = '10';
    
    $config = $this->config('flexible_google_cse.settings');

    $searchPage = [
      'result_prefix' => [
        '#type' => 'processed_text',
        '#text' => $config->get('result_prefix')['value'],
        '#format' => $config->get('result_prefix')['format'],
        '#weight' => '5',
      ],
      'results' => $searchResult,
      'result_suffix' => [
        '#type' => 'processed_text',
        '#text' => $config->get('result_suffix')['value'],
        '#format' => $config->get('result_suffix')['format'],
        '#weight' => '15',
      ],
    ];
    return $searchPage;
  }

}
